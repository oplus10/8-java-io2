package com.example.task01;

import java.io.*;

public class Task01Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:

        System.out.println(extractSoundName(new File("task01/src/main/resources/3727.mp3")));
    }

    public static String extractSoundName(File file) throws IOException, InterruptedException {
        if (file == null)
            throw new IllegalArgumentException("The transferred file object is empty");

        if (!file.exists())
            throw new FileNotFoundException("The transferred file does not exist");

        try {
            String[] cmd = {
                    "ffprobe",
                    "-loglevel", "error",
                    "-show_entries",
                    "format_tags=title",
                    "-of", "default=noprint_wrappers=1:nokey=1",
                    file.getAbsolutePath()
            };


            Process process = Runtime.getRuntime().exec(cmd);
            BufferedReader buffer = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String title = buffer.readLine();

            int exitVal = process.waitFor();
            if (exitVal == 0) {
                return title;
            } else {
                return title = "Error in the generated ffprobe process";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return "sound name";
    }
}
