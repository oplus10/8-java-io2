package com.example.task02;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.ArrayList;
import java.util.List;

import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Task02Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:


        System.out.println(listFiles(Paths.get("task02/src/main/resources/")));


    }

    public static List<Path> listFiles(Path rootDir) throws IOException, InterruptedException {
        if (rootDir == null)
            throw new IllegalArgumentException("The transferred value directory is null");
        if (!Files.exists(rootDir))
            throw new FileNotFoundException("The transferred directory does not exist");

        Stream<Path> directoryStream = Files.walk(rootDir);
        List<Path> files = directoryStream.filter(Files::isRegularFile).collect(Collectors.toList());

        directoryStream.close();

        return files;
    }
}
