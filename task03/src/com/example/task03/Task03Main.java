package com.example.task03;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

public class Task03Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:

        InputStream inputStream = new FileInputStream("task03/src/main/resources/example1.bin");
        System.out.println(deserialize(inputStream));
        inputStream.close();

    }

    public static SampleData deserialize(InputStream inputStream) throws IOException, ClassNotFoundException {
        if (inputStream == null)
            throw new IllegalArgumentException("Transferred InputStream is null");

        try (ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {
            return (SampleData) objectInputStream.readObject();
        }
    }
}
